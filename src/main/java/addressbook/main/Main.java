package addressbook.main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import addressbook.model.Address;
import addressbook.model.Person;

public class Main {

	private static EntityManagerFactory entityManagerFactory;
	private static EntityManager entityManager;

	public static void main(String[] args) {

		entityManagerFactory = Persistence.createEntityManagerFactory("AddressBook");
		entityManager = entityManagerFactory.createEntityManager();

		Person person1 = createPerson("Jan", "Kowalski", 30);
		Address address1 = createAddress("111 222 333", "jan@mail.com", "Wroclaw", "Poland");
		Person person2 = createPerson("Anna", "Nowak", 35);
		Address address2 = createAddress("123 345 678", "anna@mail.com", "Wroclaw", "Poland");
		Person person3 = createPerson("Michal", "Zielinski", 27);
		Address address3 = createAddress("100 200 300", "michal@mail.com", "Warszawa", "Poland");

		addAddress(person1, address1);
		addAddress(person2, address2);
		addAddress(person3, address3);

		makeTransaction(person1, address1);
		makeTransaction(person2, address2);
		makeTransaction(person3, address3);
	}

	private static void addAddress(Person person, Address address) {
		person.setAddress(address);
	}

	private static Address createAddress(String telNumber, String email, String city, String country) {
		Address address = new Address();
		address.setTelNumber(telNumber);
		address.setEmail(email);
		address.setCity(city);
		address.setCountry(country);
		return address;
	}

	private static Person createPerson(String firstName, String lastName, int age) {
		Person person = new Person();
		person.setFirstName(firstName);
		person.setLastName(lastName);
		person.setAge(age);
		return person;
	}

	private static void makeTransaction(Person person, Address address) {
		persistAddress(address);
		persistPerson(person);
	}

	private static void persistPerson(Person person) {
		entityManager.getTransaction().begin();
		entityManager.persist(person);
		// entityManager.persist(address);
		entityManager.getTransaction().commit();
	}

	private static void persistAddress(Address address) {
		entityManager.getTransaction().begin();
		entityManager.persist(address);
		entityManager.getTransaction().commit();
	}
}
