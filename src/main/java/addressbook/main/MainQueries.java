package addressbook.main;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import addressbook.model.Person;

public class MainQueries {

	private static EntityManagerFactory entityManagerFactory;
	private static EntityManager entityManager;

	public static void main(String[] args) {

		entityManagerFactory = Persistence.createEntityManagerFactory("AddressBook");
		entityManager = entityManagerFactory.createEntityManager();

		String queryString1 = "select p from Person p where p.lastName = 'Kowalski'";
		String queryString2 = "select p from Person p where age between 20 and 70";
		Class<Person> personClass = Person.class;

		List<Person> resultsList1 = getResults(queryString1, personClass);
		List<Person> resultsList2 = getResults(queryString2, personClass);

		showData(resultsList1);
		showData(resultsList2);
	}

	private static void showData(List<Person> resultList1) {
		getTitle();

		for (Person person : resultList1) {
			System.out.println("Name: " + person.getFirstName());
			System.out.println("last name: " + person.getLastName());
			System.out.println("age: " + person.getAge());
			System.out.println("tel: " + person.getAddress().getTelNumber());
			System.out.println("email: " + person.getAddress().getEmail());
			System.out.println("city: " + person.getAddress().getCity());
			System.out.println("country: " + person.getAddress().getCountry());
			System.out.println();
		}
	}

	private static void getTitle() {
		System.out.println("Query results:\n");
	}

	private static TypedQuery<Person> createQuery(String queryString, Class<Person> personClass) {
		TypedQuery<Person> query = entityManager.createQuery(queryString, personClass);
		return query;
	}

	private static List<Person> getResults(String queryString, Class<Person> personClass) {
		TypedQuery<Person> query = createQuery(queryString, personClass);
		List<Person> resultList = query.getResultList();
		return resultList;
	}

}
